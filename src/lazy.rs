use crate::{DbHolder, Entity, IdFor, Record};
use diesel::associations::HasTable;
use diesel::query_builder::{Query, QueryFragment, QueryId};
use diesel::query_dsl::methods::{FindDsl};
use diesel::sql_types::HasSqlType;
use diesel::{Connection, Identifiable, QueryDsl, QueryResult, RunQueryDsl};
use std::marker::PhantomData;

/// Trait for lazy entities. In general a lazy entity should be as restrained as possible.
///
/// Any lazy entity is required to contain the id (primary key) of the record and a reference to a data source.
///
/// ```rust
/// use diesel::Connection;
/// use diesel_entities_derives::{IdFor, DbHolder, LazyEntity};
///
/// #[derive(IdFor, DbHolder, LazyEntity)]
/// #[record_type(Person)]
/// #[table_name(persons)]
/// pub struct LazyPersonImpl<'db, DB: Connection> {
///     id: u32,
///     db: &'db DB,
/// }
/// ```
///
/// If a record is in one or more `belongs_to` relations those references can also be used in the lazy type.
/// The derive macro will automatically create the proper clauses for the query.
///
/// > The derive macro will currently use only the columns present in the current table.
/// > In a future version it will be possible to use joins to perform more complicated but also more secure queries.
///
/// ```rust
/// use diesel::Connection;
/// use diesel_entities_derives::{IdFor, DbHolder, LazyEntity};
///
/// #[derive(IdFor, DbHolder, LazyEntity)]
/// #[record_type(House)]
/// #[table_name(houses)]
/// pub struct LazyHouseImpl<'db, DB: Connection> {
///     id: u32,
///     db: &'db DB,
///     #[column_name(owner_id)]
///     owner_id: Option<u32>,
/// }
/// ```
pub struct LazyEntity<'db, R, DB, Id>
where
    R: Record,
    for<'r> &'r R: Identifiable<Id = &'r Id>,
    DB: Connection,
{
    id: Id,
    db: &'db DB,
    phantom: PhantomData<R>,
    //predicate: Option<Box<dyn Predicate<R>>>,
}

impl<R, DB, Id> IdFor<R> for LazyEntity<'_, R, DB, Id>
where
    R: Record,
    for<'r> &'r R: Identifiable<Id = &'r Id>,
    Id: Clone,
    DB: Connection,
{
    fn record_id(&self) -> <&R as Identifiable>::Id {
        &self.id
    }
}

impl<'db, R, DB, Id> DbHolder<'db, DB> for LazyEntity<'db, R, DB, Id>
where
    R: Record,
    for<'r> &'r R: Identifiable<Id = &'r Id>,
    DB: Connection,
{
    fn database(&self) -> &'db DB {
        self.db
    }
}

impl<'db, R, DB, Id> LazyEntity<'db, R, DB, Id>
where
    R: Record,
    for<'r> &'r R: Identifiable<Id = &'r Id>,
    DB: Connection,
{
    /// Create a new lazy entity uniquely identified by `id`.
    pub fn new(id: Id, db: &'db DB) -> Self {
        LazyEntity {
            id,
            db,
            phantom: PhantomData,
            //predicate: None,
        }
    }
    /// The id of the lazy entity.
    pub fn id(&self) -> &Id {
        &self.id
    }
}

impl<'db, R, DB, Id> LazyEntity<'db, R, DB, Id>
where
    Self: IdFor<R> + DbHolder<'db, DB>,
    R: Record,
    for<'r> &'r R: Identifiable<Id = &'r Id>,
    DB: Connection,
{
    /// Resolve the lazy entity to a concrete entity by querying the database.
    pub fn resolve<'s>(&'s self) -> QueryResult<Entity<'db, R, DB>>
    where
        R: diesel::Queryable<<<<R as HasTable>::Table as diesel::query_dsl::filter_dsl::FindDsl<&'s Id>>::Output as diesel::query_builder::Query>::SqlType, <DB as diesel::Connection>::Backend>,
        <DB as Connection>::Backend: HasSqlType<<<<R as HasTable>::Table as FindDsl<&'s Id>>::Output as Query>::SqlType>,
        <R as HasTable>::Table: FindDsl<&'s Id>,
        <<R as HasTable>::Table as FindDsl<&'s Id>>::Output: RunQueryDsl<DB> + Query + QueryFragment<<DB as Connection>::Backend> + QueryId + QueryDsl,
    {
        let find = FindDsl::find(<R as HasTable>::table(), &self.id);

        let result = find.get_result(self.database());

        Entity::map_result(result, self.database())
    }
}
