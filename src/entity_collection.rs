use crate::{Entity, MutRefEntity, RefEntity};
use diesel::{Connection, QueryResult};
use std::marker::PhantomData;



/// Owning collection of records with a reference to a database.
///
/// An entity collection offers iterators to iterate over the records.
/// Iteration can occur by reference, mutable reference or directly on the entity.
pub struct EntityCollection<'db, R, DB>
where
    DB: Connection,
{
    collection: Vec<R>,
    db: &'db DB,
}

impl<'db, R, DB> EntityCollection<'db, R, DB>
where
    DB: Connection,
{
    /// Creates a reference iterator over the records in the collection.
    pub fn iter<'r>(&'r self) -> impl Iterator<Item = RefEntity<'r, 'db, R, DB>> {
        EntityRefIterator {
            db: self.db,
            iter: self.collection.iter(),
            _phantom_r: Default::default(),
        }
    }

    /// Creates a mutable reference iterator over the records in the collection.
    pub fn iter_mut<'r>(&'r mut self) -> impl Iterator<Item = MutRefEntity<'r, 'db, R, DB>> {
        EntityMutRefIterator {
            db: self.db,
            iter: self.collection.iter_mut(),
            _phantom_r: Default::default(),
        }
    }

    /// Map a `QueryResult` to an entity collection.
    ///
    /// If `result` contains an `Err` variant, the error will be passed to the result of this function.
    pub fn map_result(result: QueryResult<Vec<R>>, db: &'db DB) -> QueryResult<Self> {
        match result {
            Ok(vec) => Ok(EntityCollection {
                collection: vec,
                db,
            }),
            Err(e) => Err(e),
        }
    }
    /// Returns the internal vector of the collection and consumes the collection-
    pub fn into_vec(self) -> Vec<R> {
        self.collection
    }
    /// See [Vec::pop].
    pub fn pop(&mut self) -> Option<Entity<'db, R, DB>> {
        self.collection
            .pop()
            .map(|record| Entity::new(record, self.db))
    }
    /// Returns the length of the collection.
    pub fn len(&self) -> usize {
        self.collection.len()
    }
    /// Returns true if the collection is empty.
    pub fn is_empty(&self) -> bool {
        self.collection.is_empty()
    }
    /// Returns a reference entity to the first element of the collection
    pub fn first<'r>(&'r self) -> Option<RefEntity<'r, 'db, R, DB>> {
        self.collection
            .first()
            .map(|ref_record| RefEntity::new(ref_record, self.db))
    }
}

/// Struct that represents an iterator over a collection of records which are encapsulated with a reference to a database.
pub struct EntityIterator<'db, R, I, DB>
where
    I: Iterator<Item = R>,
    DB: Connection,
{
    db: &'db DB,
    iter: I,
}

impl<'db, R, I, DB> Iterator for EntityIterator<'db, R, I, DB>
where
    DB: Connection,
    I: Iterator<Item = R>,
{
    type Item = Entity<'db, R, DB>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|record| Entity::new(record, self.db))
    }
}

/// Struct to iterate over a collection of records using reference entities.
pub struct EntityRefIterator<'r, 'db, R, I, DB>
where
    I: Iterator<Item = &'r R>,
    DB: Connection,
{
    db: &'db DB,
    iter: I,
    _phantom_r: PhantomData<&'r R>,
}

impl<'r, 'db, R, I, DB> Iterator for EntityRefIterator<'r, 'db, R, I, DB>
where
    DB: Connection,
    I: Iterator<Item = &'r R>,
{
    type Item = RefEntity<'r, 'db, R, DB>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter
            .next()
            .map(|record| RefEntity::new(record, self.db))
    }
}

/// Struct to iterate over a collection of records using mutable reference entities.
pub struct EntityMutRefIterator<'r, 'db, R, I, DB>
where
    I: Iterator<Item = &'r mut R>,
    DB: Connection,
{
    db: &'db DB,
    iter: I,
    _phantom_r: PhantomData<&'r R>,
}

impl<'r, 'db, R, I, DB> Iterator for EntityMutRefIterator<'r, 'db, R, I, DB>
where
    DB: Connection,
    I: Iterator<Item = &'r mut R>,
{
    type Item = MutRefEntity<'r, 'db, R, DB>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter
            .next()
            .map(|record| MutRefEntity::new(record, self.db))
    }
}

impl<'db, R, DB> IntoIterator for EntityCollection<'db, R, DB>
where
    DB: Connection,
{
    type Item = Entity<'db, R, DB>;
    type IntoIter = EntityIterator<'db, R, <Vec<R> as IntoIterator>::IntoIter, DB>;

    fn into_iter(self) -> Self::IntoIter {
        let iter = self.collection.into_iter();
        EntityIterator { db: self.db, iter }
    }
}

impl<R, DB> Into<Vec<R>> for EntityCollection<'_, R, DB>
where
    DB: Connection,
{
    fn into(self) -> Vec<R> {
        self.collection
    }
}
