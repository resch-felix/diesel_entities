use crate::{DbHolder, IdFor, Record};
use diesel::{Connection, Identifiable};
use std::ops::Deref;

/// A reference entity contains a reference to a record and a data source.
///
/// A reference entity also implements `Deref<Target=R>` so it can be used with non modifying traits.
///
/// ```rust
/// use diesel_entities::{Record, DbHolder, IdFor};
/// use diesel::Connection;
/// use std::ops::Deref;
/// #[derive(Queryable, Identifiable)]
/// #[primary_key("name")]
/// struct Person {
///     name: String,
///     age: u32,
/// }
///
/// impl Record for Person {}
///
/// trait RefPerson<'d, D: Connection> where Self: DbHolder<'d, D> + IdFor<Person> + Deref<Target=Person> {
///     fn get_age(&self) -> u32 {
///         self.age
///     }
/// }
/// ```
pub struct RefEntity<'r, 'db, R, DB>
where
    DB: Connection,
{
    record: &'r R,
    db: &'db DB,
}

impl<'r, 'db, R, DB> RefEntity<'r, 'db, R, DB>
where
    DB: Connection,
{
    /// Create a new [RefEntity] from a reference to a record and a data source.
    pub fn new(record: &'r R, db: &'db DB) -> Self {
        RefEntity { record, db }
    }
}

impl<R, DB> IdFor<R> for RefEntity<'_, '_, R, DB>
where
    R: Record,
    for<'a> &'a R: Identifiable,
    DB: Connection,
{
    fn record_id(&self) -> <&R as Identifiable>::Id {
        self.record.record_id()
    }
}

impl<R, DB> Deref for RefEntity<'_, '_, R, DB>
where
    DB: Connection,
{
    type Target = R;

    fn deref(&self) -> &Self::Target {
        &self.record
    }
}

impl<'db, R, DB> DbHolder<'db, DB> for RefEntity<'_, 'db, R, DB>
where
    R: Record,
    for<'a> &'a R: Identifiable,
    DB: Connection,
{
    fn database(&self) -> &'db DB {
        self.db
    }
}
