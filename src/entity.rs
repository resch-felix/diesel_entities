use crate::{Consume, DbHolder, IdFor, MutRefEntity, Record, RefEntity};

use diesel::{Connection, Identifiable, QueryResult};

use diesel::query_builder::AsChangeset;
use std::fmt::{Debug, Formatter};
use std::ops::{Deref, DerefMut};

/// Wraps a record to simplify further calls which need a database connection.
///
/// To transparently implement functions for records use traits that are generically implemented for all types which implement the required traits (e.g. [IdFor], [DbHolder], ...)
///
/// This struct implements `Deref<Target=R>` and `DerefMut<Target=R>` so it can be used with traits used for [RefEntity] and [MutRefEntity].
pub struct Entity<'db, R, DB>
where
    DB: Connection,
{
    record: R,
    db: &'db DB,
}

impl<R, DB> Debug for Entity<'_, R, DB>
where
    R: Debug,
    DB: Connection,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        (&self.record as &dyn Debug).fmt(f)
    }
}

impl<'db, R, DB> Entity<'db, R, DB>
where
    DB: Connection,
{
    /// Create a new entity from a record and a reference to a datasource.
    pub fn new(record: R, db: &'db DB) -> Self {
        Entity { record, db }
    }

    /// Map the result of a query which produces a single record to an entity.
    pub fn map_result(result: QueryResult<R>, db: &'db DB) -> QueryResult<Self> {
        match result {
            Ok(record) => Ok(Self::new(record, db)),
            Err(e) => Err(e),
        }
    }

    /// Return the inner record.
    ///
    /// Please note that this removes the reference to the database.
    pub fn into_inner(self) -> R {
        self.record
    }

    /// Create a reference entity from this entity.
    pub fn to_ref<'s>(&'s self) -> RefEntity<'s, 'db, R, DB> {
        RefEntity::new(&self.record, self.db)
    }

    /// Create a mutable reference entity from this entity.
    pub fn to_mut_ref<'s>(&'s mut self) -> MutRefEntity<'s, 'db, R, DB> {
        MutRefEntity::new(&mut self.record, self.db)
    }

    // pub fn update(&mut self) -> QueryResult<()>
    //     where
    //         <R as HasTable>::Table: Identifiable,
    //         for<'r> Find<<R as HasTable>::Table, <&'r R as Identifiable>::Id>: IntoUpdateTarget + Sized,
    // {
    //     use diesel::QueryDsl;
    //
    //     let find = FindDsl::find(R::table(),&self.record_id());
    //     let update = diesel::update(find).set(&self);
    //
    //     // let record = diesel::update(&R::table().find(&self.record_id()))
    //     //     .set(&self)
    //     //     .get_result(self.database())?;
    //     //
    //     // self.record = record;
    //     Ok(())
    // }
}

impl<R, DB> IdFor<R> for Entity<'_, R, DB>
where
    R: Record,
    for<'r> &'r R: Identifiable,
    DB: Connection,
{
    fn record_id(&self) -> <&R as Identifiable>::Id {
        self.record.record_id()
    }
}

impl<R, DB> Deref for Entity<'_, R, DB>
where
    DB: Connection,
{
    type Target = R;

    fn deref(&self) -> &Self::Target {
        &self.record
    }
}

impl<R, DB> DerefMut for Entity<'_, R, DB>
where
    DB: Connection,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.record
    }
}

impl<'db, R, DB> DbHolder<'db, DB> for Entity<'db, R, DB>
where
    DB: Connection,
{
    fn database(&self) -> &'db DB {
        self.db
    }
}

impl<R, DB: Connection> Consume<R> for Entity<'_, R, DB> {
    fn consume(self) -> R {
        self.record
    }
}

impl<R, DB: Connection> AsChangeset for Entity<'_, R, DB>
where
    R: AsChangeset,
{
    type Target = <R as AsChangeset>::Target;
    type Changeset = <R as AsChangeset>::Changeset;

    fn as_changeset(self) -> Self::Changeset {
        self.record.as_changeset()
    }
}
