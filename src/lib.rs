#![feature(associated_type_defaults)]
#![warn(clippy::pedantic)]
#![warn(missing_docs)]

//! This crate aims to provide generic types and traits to simplify development of applications with [diesel](https://diesel.rs/).
//!
//! In this crate the two concepts for "a row in a database" are used:
//!
//! * **Records** a record is a struct containing the structured data from one row as diesel would deserialize it
//! * **Entities** an entity is a record together with a reference to a data source (database) to simplify method calls
//!
//! Entities are then further divided into:
//!
//! * **Lazy Entities** a lazy entity contains only an unique (tuple) identifier such as the primary key and a reference to a data source.
//!     All types that have such an identifier and a reference to a data source are regarded as a lazy entity, even though they might have been loaded eagerly.
//! * **(Normal) Entities** a normal entity owns the underlying record.
//! * **Ref/Mut Ref Entities** reference or mutable reference entities own a (mutable) reference to the underlying record. These types are mostly used for iterating entity collections.
//!
//! Methods that work on entities should be written in separate traits which are implemented using blanket implementations. The traits to work with a simple Person struct could look like this:
//!
//! ```rust
//! use diesel::{Queryable, Identifiable, table};
//!
//! use diesel_entities::{IdFor, DbHolder, Record};
//! use diesel::prelude::*;
//! use std::ops::{Deref, DerefMut};
//!
//! table! {
//!     persons (name) {
//!         name -> Varchar,
//!         age -> Int8,
//!     }
//! }
//!
//! #[derive(Queryable, Identifiable)]
//! #[primary_key("name")]
//! struct Person {
//!     name: String,
//!     age: u32,
//! }
//!
//! impl Record for Person {}
//!
//! trait RefPerson<'d, D: Connection> where Self: DbHolder<'d, D> + IdFor<Person> + Deref<Target=Person> {
//!     fn get_age(&self) -> u32 {
//!         self.age
//!     }
//! }
//!
//! trait MutRefPerson<'d, D: Connection> where Self: DbHolder<'d, D> + IdFor<Person> + DerefMut<Target=Person> {
//!     fn set_age(&mut self, new_age: u32) {
//!         self.age = new_age
//!     }
//! }
//! ```

mod entity;
mod entity_collection;
mod lazy;
mod mut_ref_entity;
mod ref_entity;
//mod composite;

pub use entity::Entity;
pub use entity_collection::EntityCollection;
pub use lazy::LazyEntity;
pub use mut_ref_entity::MutRefEntity;
pub use ref_entity::RefEntity;

use diesel::associations::HasTable;
use diesel::backend::SupportsReturningClause;
use diesel::insertable::CanInsertInSingleQuery;
use diesel::query_builder::{IncompleteInsertStatement, InsertStatement, QueryFragment};
use diesel::sql_types::HasSqlType;
use diesel::{
    Connection, Identifiable, Insertable, QueryResult, QuerySource, Queryable, RunQueryDsl, Table,
};

/// Any type implementing this trait holds an identifier for type `R`.
///
/// The type used in `R` needs to be identifiable so the type of the id can be known.
pub trait IdFor<R>
where
    R: Record,
    for<'r> &'r R: Identifiable,
{
    /// Return the id of a record for the type `R`.
    fn record_id(&self) -> <&R as Identifiable>::Id;
}

/// General trait for a record, this trait combines traits which are required for records.
pub trait Record
where
    Self: HasTable + Sized,
    for<'r> &'r Self: Identifiable,
{
    /// Insert new records into this entity type and return the inserted records.
    fn insert_many<'db, U, DB: Connection>(
        records: U,
        db: &'db DB,
    ) -> QueryResult<EntityCollection<'db, Self, DB>>
    where
        <DB as Connection>::Backend: SupportsReturningClause,
        U: Insertable<<Self as HasTable>::Table>,
        <<Self as HasTable>::Table as QuerySource>::FromClause:
            QueryFragment<<DB as Connection>::Backend>,
        <U as Insertable<<Self as HasTable>::Table>>::Values: QueryFragment<<DB as Connection>::Backend>
            + CanInsertInSingleQuery<<DB as Connection>::Backend>,
        <<Self as HasTable>::Table as Table>::AllColumns:
            QueryFragment<<DB as Connection>::Backend>,
        <DB as Connection>::Backend: HasSqlType<
            <<<Self as HasTable>::Table as Table>::AllColumns as diesel::Expression>::SqlType,
        >,
        Self: Queryable<
            <<<Self as HasTable>::Table as Table>::AllColumns as diesel::Expression>::SqlType,
            <DB as Connection>::Backend,
        >,
    {
        let incomplete_insert_statement: IncompleteInsertStatement<<Self as HasTable>::Table, _> =
            diesel::insert_into(<Self as HasTable>::table());
        let insert_statement: InsertStatement<
            <Self as HasTable>::Table,
            <U as Insertable<<Self as HasTable>::Table>>::Values,
        > = incomplete_insert_statement.values(records);
        let result: QueryResult<Vec<Self>> = insert_statement.get_results(db);

        EntityCollection::map_result(result, db)
    }

    /// Insert a new record into this entity type and returns the inserted record.
    fn insert<'db, U, T: Table, DB: Connection>(
        table: T,
        record: U,
        db: &'db DB,
    ) -> QueryResult<Entity<'db, Self, DB>>
    where
        <DB as Connection>::Backend: SupportsReturningClause,
        U: Insertable<T>,
        <T as QuerySource>::FromClause: QueryFragment<<DB as Connection>::Backend>,
        <U as Insertable<T>>::Values: QueryFragment<<DB as Connection>::Backend>
            + CanInsertInSingleQuery<<DB as Connection>::Backend>,
        <T as Table>::AllColumns: QueryFragment<<DB as Connection>::Backend>,
        <DB as Connection>::Backend:
            HasSqlType<<<T as Table>::AllColumns as diesel::Expression>::SqlType>,
        Self: Queryable<
            <<T as Table>::AllColumns as diesel::Expression>::SqlType,
            <DB as Connection>::Backend,
        >,
    {
        let incomplete_insert_statement: IncompleteInsertStatement<T, _> =
            diesel::insert_into(table);
        let insert_statement: InsertStatement<T, <U as Insertable<T>>::Values> =
            incomplete_insert_statement.values(record);
        let result: QueryResult<Self> = insert_statement.get_result(db);

        Entity::map_result(result, db)
    }
}

impl<R> IdFor<R> for R
where
    R: Record,
    for<'r> &'r R: Identifiable,
{
    fn record_id(&self) -> <&R as Identifiable>::Id {
        self.id()
    }
}

/// General trait for components that contain a reference to a data source.
///
/// This trait is implemented by all types of entity and constrains them to be used only in the presence of one.
/// To retrieve the record either use [LazyEntity](crate::lazy::LazyEntity::resolve) or [Entity::into_inner](crate::entity::Entity::into_inner).
/// Referencing entities ([RefEntity](crate::ref_entity::RefEntity) or [MutRefEntity](crate::mut_ref_entity::MutRefEntity)) cannot be used to retrieve the records.
pub trait DbHolder<'db, DB>
where
    DB: Connection,
{
    /// Returns a reference to the data source this object is holding.
    fn database(&self) -> &'db DB;
}

/// Allows to consume a type and produce an instance of `R`.
///
/// Alternative to [Into] as there would be conflicting implementations.
pub trait Consume<R>: Sized {
    /// Consume the value and return an instance of type `R`.
    fn consume(self) -> R;
}
