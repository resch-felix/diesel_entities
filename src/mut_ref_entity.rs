use crate::{DbHolder, IdFor, Record};
use diesel::{Connection, Identifiable};
use std::ops::{Deref, DerefMut};

/// A mutable reference entity contains a mutable reference to a record and a data source.
///
/// A mutable reference entity implements `Deref<Target=R>` and `DerefMut<Target=R>` so it can be used with non-modifying and modifying traits.
pub struct MutRefEntity<'r, 'db, R, DB>
where
    DB: Connection,
{
    record: &'r mut R,
    db: &'db DB,
}

impl<'r, 'db, R, DB> MutRefEntity<'r, 'db, R, DB>
where
    DB: Connection,
{
    /// Create a new mutable reference entity from a mutable reference and a reference to a diesel database.
    pub fn new(record: &'r mut R, db: &'db DB) -> Self {
        MutRefEntity { record, db }
    }
}

impl<R, DB> IdFor<R> for MutRefEntity<'_, '_, R, DB>
where
    R: Record,
    for<'r> &'r R: Identifiable,
    DB: Connection,
{
    fn record_id(&self) -> <&R as Identifiable>::Id {
        self.record.record_id()
    }
}

impl<R, DB> Deref for MutRefEntity<'_, '_, R, DB>
where
    DB: Connection,
{
    type Target = R;

    fn deref(&self) -> &Self::Target {
        &self.record
    }
}

impl<R, DB> DerefMut for MutRefEntity<'_, '_, R, DB>
where
    DB: Connection,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.record
    }
}

impl<'db, R, DB> DbHolder<'db, DB> for MutRefEntity<'_, 'db, R, DB>
where
    R: Record,
    for<'r> &'r R: Identifiable,
    DB: Connection,
{
    fn database(&self) -> &'db DB {
        self.db
    }
}
